resource "aws_vpc" "vpc_1" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_hostnames =  var.enable_dns_hostnames

  tags = {
    Name = "firstvpc"
  }
}

resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.vpc_1.id
  cidr_block = var.subnet_cidr

  tags = {
    Name = "Main"
  }
}


resource "aws_route_table" "route1" {
  vpc_id = aws_vpc.vpc_1.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "main"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc_1.id

  tags = {
    Name = "main"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet_1.id
  route_table_id = aws_route_table.route1.id
}